/*function defaultTask(cb) {
// place code for your default task here
cb();
}

exports.default = defaultTask*/

const gulp = require('gulp');
var postcss = require('gulp-postcss');
const tailwindcss = require('tailwindcss');


const autoprefixer = require('gulp-autoprefixer');
gulp.task('styles', () => {
    return gulp
        .src('./src/styles/tailwind.css')

    .pipe(postcss([
            tailwindcss('./tailwind.config.js'),
            require('autoprefixer')

        ]))
        .pipe(gulp.dest("dist/css"))

});
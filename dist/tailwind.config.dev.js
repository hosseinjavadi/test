"use strict";

module.exports = {
  theme: {
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px"
    },
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: "#000",
      white: "#fff",
      green: "#4AC49E",
      red: "#FF6464",
      yellow: "#FFE32E",
      gray: "#B0B1C5",
      lightblue: "#98A0FF",
      primaryblue: "#434FD9",
      secondaryblue: "#323FCE"
    },
    boxShadow: {
      "2xl": " 0 12px 25px -6px rgba(0,0,0,0.25)",
      "3xl": " 0 25px 50px -12px rgba(0, 0, 0, 0.55)"
    },
    borderRadius: {
      "default": "0.25rem",
      none: "0",
      sm: "0.125rem",
      md: "0.375rem",
      lg: "0.5rem",
      full: "9999px",
      xl: "1rem"
    }
  },
  backgroundColor: function backgroundColor(theme) {
    return theme("colors");
  }
};